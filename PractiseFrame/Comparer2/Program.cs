﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparer2
{
    internal class Program
    {
        public class Person : IComparable<Person>
        {
            private string name;
            private int age;
            public Person(int age, string name)
            {
                this.name = name;
                this.age = age;
            }   
            public string Name { get { return name; } }
            public int Age { get { return age; } }  
            public int CompareTo(Person person)
            {
                if (person==null)
                {
                    return 1;
                }
                if (string.Compare(this.name, person.Name)==1)
                {
                    return 1;
                }

                if (string.Compare(this.name, person.Name) == -1)
                {
                    return -1;
                }
                return this.age.CompareTo(person.Age);
            }
        }
        static void Main(string[] args)
        {
            List<Person> persons = new List<Person>() { new Person(10, "Gosho"), new Person(18, "Todor")};
            Person person1 = new Person(40, "Ivan");
            Person person2 = new Person(50, "Tsvetan");
            Person person4 = new Person(10, "Tsvetan");
            Person person3 = new Person(20, "Peter");
            Person person5 = new Person(12, "Peter");
            persons.Add(person1);
            persons.Add(person2);
            persons.Add(person3);
            persons.Add(person4);
            persons.Add(person5);   
            persons.Sort();
            foreach (var item in persons)
            {
                Console.WriteLine(item.Name + item.Age);
            }
            Console.Read();
        }
    }
}
