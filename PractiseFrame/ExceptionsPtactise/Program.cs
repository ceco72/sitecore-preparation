﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionsPtactise
{
    internal class Program
    {

public class Person
    {
        private string _name;
            public Person(string name)
            {
                this._name = name;
            }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
                Person person = obj as Person;
                if (person == null)
                {
                    return false;
                }
                return this.Name.Equals(person.Name);
        }
    }


    public class Example
    {
        public static void Main()
        {
            Person p1 = new Person("John");
            Person p2 = new Person("John");

            Console.WriteLine("p1 = p2: {0}", p1.Equals(p2));
                Console.ReadLine();
        }
    }
    // The example displays the following output:
    //        p1 = p2: False
}
}
