﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yield3
{
    internal class Program
    {
        public static IEnumerable<int> Numbers(int max)
        {
            for (int i = 0; i < max; i++)
            {
                Console.WriteLine("Returning {0}", i);
                yield return i;
            }
        }
        public static IEnumerable <int> Numbers2 (int min)
        {
            for ( int i = 0; i> min; i --)
            {
                Console.WriteLine($"Number: {i}");
                yield return i;
            }
        }
        static void Main(string[] args)
        {
            foreach (var item in Numbers(10).Take(3))
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();

            foreach (var item in Numbers2(-5))
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
