﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PractiseFrame
{
    class Program

    {

        static void Main(string[] args)

        {

            A a = new A();

            A c = new C();
            Console.Read();

        }

    }



    public class A

    {

        public A() : this("s1", "s2") => Console.WriteLine("1");

        public A(string s1, string s2) : this("s1", "s2", "s3") => Console.WriteLine("2");

        public A(string s1, string s2, string s3) => Console.WriteLine("3");

    }



    public class B : A

    {

        public B() : base() => Console.WriteLine("4");

    }



    public class C : B

    {

        public C() : base() => Console.WriteLine("5");

    }








}
