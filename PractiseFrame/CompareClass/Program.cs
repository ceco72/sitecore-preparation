﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareClass
{
    public class Person : IComparable<Person>

    {
        private int age;
        private string name;    
        public Person(int age, string name)
        {
            this.age = age;
            this.name = name;
           
        }

        public string Name { get { return this.name; } }

        public int Age { get { return this.age; } }

        public int CompareTo(Person p)

        {

            return this.Age.CompareTo(p.Age);

        }

    }


    internal class Program
    {
        //public class Person : Comparer<Person>
        //{
        //    private int age;
        //    private string name;    
        //    public Person(int age, string name)
        //    {
        //        this.age = age;
        //        this.name = name;
        //    }   
        //    public int Age { get { return age; } }
        //    public string Name { get { return name; } }

        //    public override int Compare(Person person1, Person person2)
        //    {
        //        if (person1.Age.CompareTo(person2.Age)!=0)
        //        {
        //            return person1.Age.CompareTo(person2.Age);
        //        }
        //        else return 0;
                
        //        //if (other.Age>person.Age)
        //        //{
        //        //    return -1;
        //        //} else if (other.Age < person.Age)
        //        //{
        //        //    return 1;
        //        //}
        //        //else
        //        //{
        //        //    return 0;
        //        //}
        //    }
        //}
        static void Main(string[] args)
        {
            List<Person> myList = new List<Person>();
            Person person1 = new Person(40, "Ivan");
            Person person2 = new Person(50, "Tsvetan");
            Person person3 = new Person(20, "Peter");
            myList.Add(person1); myList.Add(person2);myList.Add(person3);
            myList.Sort();  
                foreach (var item in myList)
            {
                Console.WriteLine(item.Name);
            }
            Console.ReadLine(); 
        }
    }
}
