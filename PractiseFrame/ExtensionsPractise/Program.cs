﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionsPractise
{
    public static class MyExtensions
    {
        public static string Revert(this string str)
        {
            StringBuilder strb = new StringBuilder();
            for (int i = str.Length-1; i >= 0; i--)
            {
                strb.Append(str[i]);
            }
            return strb.ToString();
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 3, 2, 1, 7 };
            var arr1 = arr.OrderBy(x => x);
            foreach (var item in arr1)
            {
                Console.WriteLine(item);
            }
            var arr2 = arr.Min(x => x);
            Console.WriteLine(arr2);

            string football = "football";

            Console.WriteLine(football.Revert());
            var res = MyExtensions.Revert(football);
            var res1 = football.Revert();
            Console.WriteLine(res);

            Console.ReadLine();
        }
        
    }
}
