﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yield2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<int> ints = new List<int>() { 4, 6, 10, -2, 10, 40};
            
            //int [] arr = new int[] { 3, 7, 10 };
            Console.WriteLine(string.Join(" ", TakeWhilePositive(ints)));
            // Output: 9 8 7
            Console.Read();
            IEnumerable<int> TakeWhilePositive(IEnumerable<int> numbers)
            {
                foreach (int n in numbers)
                {
                    if (n > 0)
                    {
                        yield return n;
                    }
                    else
                    {
                        yield break;
                    }
                }
            }
        }
    }
}
