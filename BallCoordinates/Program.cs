﻿using BallCoordinates.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using static System.Net.Mime.MediaTypeNames;
namespace BallCoordinates
{
    class Program
    {
    //    There is a ball that could moveat any point on XY coordinates(10x10).Implement a simple console application that moves
    //    a ball a few times and outputs the value of the current position.In case the ball crashesinto the wall(X= 0 or Y = 0) or
    //    appears behind it(X or Y a less than 0) the program should throw an exception.Catch and log the exception in the console
    //    and stop moving the ball.Also output the entire path of the ball till the exception is thrown.
    //    Requirements:
    //    1.Each new point for the ball must be logged to a text file.
    //    2.The ball gets the random coordinates every time.Additional notes:Implement a custom exception.
    //    It should have an overridden Message field that returns the entire path for a ball from the text file.



        static void Main(string[] args)
        {
            List<string> allCoor = new List<string>();
            List<string> allCoorFile = new List<string>();
            string filePath = @"C:\Users\ivan\Desktop\Practice\SCPrep\record.txt";
            try
            {
                Random rand = new Random();
                while (true)
                {
                    int x = rand.Next(0, 12);
                    int y = rand.Next(0, 11);
                    StringBuilder sb = new StringBuilder();
                    if (x <= 0 || x > 9 || y <= 0 || y > 10)
                    {
                        allCoorFile = File.ReadAllLines(filePath).ToList();
                        foreach (var item in allCoorFile)
                        {
                            sb.Append(item);
                        }
                        throw new CoordException("Here is the path of ball: " + sb.ToString());
                    }
                    else
                    {
                        Console.WriteLine($"x is {x} and y is {y}");
                        allCoor.Add("(" + x + ", " + y + ")");
                        try
                        {
                            File.WriteAllLines(filePath, allCoor);

                        }
                        catch (Exception)
                        {

                            Console.WriteLine("Oopssss...The file is being used in another program..."); ;
                        }
                    }
                }
            }
            catch (CoordException e)
            {
                Console.WriteLine("The Ball has hit a wall...");
                Console.WriteLine(e);
                //foreach (var item in allCoorFile)
                //{
                //    Console.WriteLine(item);
                //}

            }
        }
    }
}


