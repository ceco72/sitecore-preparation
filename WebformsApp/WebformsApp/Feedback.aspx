﻿<%@ Page Title="Feedback" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="WebformsApp.Feedback" %>
<%@ Register Src="~/WebUserControl1.ascx" TagPrefix="UC" TagName="User1" %>
<%--<%@ OutputCache CacheProfile="TwoDay" Duration="1" VaryByParam="none"%>--%>
  <%@ OutputCache Duration="1" VaryByParam="none" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <asp:Button ID="Button1" runat="server" Text="Logout" OnClick="Button1_Click" />
     <asp:Button ID="btnRedirect" runat="server" Onclick="btnRedirect_Click" Text="Redirect to Home Page" />
    <h1><%: Title %>.</h1>
    <UC:User1 runat="server"></UC:User1>
 </asp:Content>
