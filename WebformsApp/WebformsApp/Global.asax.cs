﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace WebformsApp
{
    public class Global : HttpApplication
    {
        public static string fullPath = @"C:\Users\ivan\Desktop\Practice\WebformsApp\WebformsApp\Uptime.txt";

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            File.AppendAllText(fullPath, $"Application starts at {DateTime.Now.ToString()}" + Environment.NewLine);

        }
        void Session_End(object sender, EventArgs e)
        {
            File.AppendAllText(fullPath, DateTime.Now.ToString() + Environment.NewLine);
            var name = Session["username"];

        }
        void Application_End(object sender, EventArgs e)
        {
            File.AppendAllText(fullPath, DateTime.Now.ToString() + Environment.NewLine);

        }
    }
}