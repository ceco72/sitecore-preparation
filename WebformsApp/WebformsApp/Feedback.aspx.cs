﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebformsApp
{
    public partial class Feedback : System.Web.UI.Page
    {
        public static bool redirected;
        protected void Page_Load(object sender, EventArgs e)
        {
            _Default.count++;
            var username = Session["username"];
            var firstname = _Default.items["firstName"];
            var lastname = _Default.items["lastName"];

            var name = HttpContext.Current.Application["name"];
        }
        protected void btnRedirect_Click(object sender, EventArgs e)
        {
            
            var collection = Request.Headers;
            string ho = collection["host"];
            if (ho == "localhost:44399")
            {
                redirected = true;
                WebUserControl1.feedback = true;

            }

            Response.Redirect("~/Default.aspx");

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            File.AppendAllText(Global.fullPath, DateTime.Now.ToString() + " this is the end" + Environment.NewLine);

            Session.Abandon();
        }
    }
}