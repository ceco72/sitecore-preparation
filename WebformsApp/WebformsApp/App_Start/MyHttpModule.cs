﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace WebformsApp.App_Start
{
    public class MyHttpModule : IHttpModule
    {
        //private StreamWriter sw;
        string fullPath = @"C:\Users\ivan\Desktop\Practice\WebformsApp\WebformsApp\Uptime.txt";
        public void Dispose()
        {
        }
                
        public void Init(HttpApplication context)
        {
            //context.BeginRequest += new EventHandler(this.Application_BeginRequest);
           // context.EndRequest += new EventHandler(this.Application_EndRequest);

        }

       private void Application_BeginRequest(Object source, EventArgs e)
        {
            File.AppendAllText(fullPath, DateTime.Now.ToString() + Environment.NewLine);

            //if (!File.Exists("Logger.txt"))
            //{
            //    sw = new StreamWriter(@"C:\Users\ivan\Desktop\Practice\France\France\Logger.txt");
            //}
            //else
            //{
            //    sw = File.AppendText("Logger.txt");
            //}

            //sw.WriteLine("User sent request at {0}", DateTime.Now);
            //sw.Close();
        }
        private void Application_EndRequest(Object source, EventArgs e)
        {
           
            File.AppendAllText(fullPath, $"End of request at: {DateTime.Now.ToString()}" + Environment.NewLine);

            //if (!File.Exists("Logger.txt"))
            //{
            //    sw = new StreamWriter(@"C:\Users\ivan\Desktop\Practice\France\France\Logger.txt");
            //}
            //else
            //{
            //    sw = File.AppendText("Logger.txt");
            //}

            //sw.WriteLine("Site has stopped at: {0}", DateTime.Now);
            //sw.Close();
        }
    }
}