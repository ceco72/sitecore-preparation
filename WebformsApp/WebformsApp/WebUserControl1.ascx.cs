﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebformsApp
{
    public partial class WebUserControl1 : System.Web.UI.UserControl
    {
        public static bool feedback = true; 
        string fullPath = @"C:\Users\ivan\Desktop\Practice\WebformsApp\WebformsApp\TextFile1.txt";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var val1 = RadioButtonList1.Items[0].Selected;
            var val2 = RadioButtonList1.Items[1].Selected;
            var val3 = RadioButtonList1.Items[2].Selected;
            string name = TextBox1.Text;
            if (val1)
            {
                File.AppendAllText(fullPath, $"User {name} finds this site lovely." + Environment.NewLine);


                //using (StreamWriter writer = new StreamWriter(fullPath))
                //{
                //    writer.WriteLine($"User {name} finds this site lovely.");
                //}
            }
            if (val2)
            {
                File.AppendAllText(fullPath, $"User {name} finds this site average." + Environment.NewLine);

                //using (StreamWriter writer = new StreamWriter(fullPath))
                //{
                //    writer.WriteLine($"User {name} finds this site average.");
                //}

            }
            if (val3)
            {
                File.AppendAllText(fullPath, $"User {name} finds this site bad." + Environment.NewLine);

                //using (StreamWriter writer = new StreamWriter(fullPath))
                //{
                //    writer.WriteLine($"User {name} finds this site bad.");
                //}
            }
            else if (!val1&&!val2&&!val3)
            {
                File.AppendAllText(fullPath, $"User {name} has not specified." + Environment.NewLine);

                //using (StreamWriter writer = new StreamWriter(fullPath))
                //{
                //    writer.WriteLine($"User {name} has not specified");
                //}
            }

            if (!val1 && !val2 && !val3)
            {
                feedback = false;
            }

        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}