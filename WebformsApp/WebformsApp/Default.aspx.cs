﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace WebformsApp
{
    public partial class _Default : Page
    {
        public static SessionStateItemCollection items = new SessionStateItemCollection();

        public static int count = 0;
       public static string fullPath = @"C:\Users\ivan\Desktop\Practice\WebformsApp\WebformsApp\\\Count.txt";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            items["firstName"] = "Dragan";
            items["lastName"] = "Donev";
            var collection = Request.Headers;
            string ho = collection["host"];
            Session["username"] = "peter";
            //Session.Add(itemName, itemValue);

            HttpContext.Current.Application["name"] = "Tsvetan";
            
            HttpCookie userInfo = new HttpCookie("userInfo");
            userInfo.Expires = DateTime.Now.AddHours(1);
            userInfo["UserName"] = "Annathurai";
            userInfo["UserColor"] = "Black";
            userInfo.Expires.Add(new TimeSpan(0, 1, 0));
            Response.Cookies.Add(userInfo);
            //Response.Cookies["userName"].Value = "Annathurai";
            //Response.Cookies["lastname"].Value = "Annand";

            if (Feedback.redirected==true)
            {
                Label1.Text = "Might you have some information to let us know? Please put it here.";
                Feedback.redirected = false;    
            }
            else
            {
                Label1.Text = "You can leave a feedback here";
            }
        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            //File.AppendAllText(fullPath, $"This user has visited the feedback page {count}  times" + Environment.NewLine);

            using (StreamWriter writer = new StreamWriter(fullPath))
            {
                writer.WriteLine($"User has visited the feedback page {count} times.");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string User_name = string.Empty;
            string User_color = string.Empty;
            HttpCookie reqCookies = Request.Cookies["userInfo"];
            if (reqCookies != null)
            {
                User_name = reqCookies["UserName"].ToString();
                User_color = reqCookies["UserColor"].ToString();
            }
            Label2.Text = User_name + " " + User_color;

            Session.Abandon();

        }
    }
}