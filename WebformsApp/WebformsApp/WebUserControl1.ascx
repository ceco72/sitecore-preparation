﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebUserControl1.ascx.cs" Inherits="WebformsApp.WebUserControl1" %>
<%@ OutputCache Duration="1" Shared="true" VaryByParam="none" VaryByControl="TextBox1" %>


<asp:Button ID="Button1" runat="server" Onclick="Button1_Click" Text="Button" />
<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
<asp:TextBox ID="TextBox1" runat="server" ViewStateMode="Disabled" EnableViewState="false"></asp:TextBox>
<asp:RadioButtonList ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" ViewStateMode="Disabled" EnableViewState="false">
    <asp:ListItem>Lovely</asp:ListItem>
    <asp:ListItem>Average</asp:ListItem>
    <asp:ListItem>Bad</asp:ListItem>
</asp:RadioButtonList>

