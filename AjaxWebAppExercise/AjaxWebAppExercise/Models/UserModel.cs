﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxWebAppExercise.Models
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public Role Role { get; set; }
    }
}