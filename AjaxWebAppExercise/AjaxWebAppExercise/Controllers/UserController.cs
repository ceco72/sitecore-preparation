﻿using AjaxWebAppExercise.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AjaxWebAppExercise.Controllers
{
    public class UserController : Controller
    {
        private readonly UserModel[] userData =
              {
         new UserModel {FirstName = "Edy", LastName = "Clooney", Role = Role.Admin, },
         new UserModel {FirstName = "David", LastName = "Sanderson", Role = Role.Admin},
         new UserModel {FirstName = "Pandy", LastName = "Griffyth", Role = Role.Normal},
         new UserModel {FirstName = "Joe", LastName = "Gubbins", Role = Role.Normal},
         new UserModel {FirstName = "Mike", LastName = "Smith", Role = Role.Guest}
      };
        // GET: User
        public ActionResult Index()
        {
            return View(userData);
        }

        public PartialViewResult GetUserData(string selectedRole = "All")
        {
            IEnumerable data = userData;

            if (selectedRole != "All")
            {
                var selected = (Role)Enum.Parse(typeof(Role), selectedRole);
                data = userData.Where(p => p.Role == selected);
            }

            return PartialView(data);
        }
        public ActionResult GetUser(string selectedRole = "All")
        {
            return View((object)selectedRole);
        }
    }
}