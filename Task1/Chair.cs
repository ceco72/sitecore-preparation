﻿using System;
using System.Collections.Generic;
using System.Text;
using Tasks.Contracts;

namespace Tasks
{
   public class Chair : Furniture
    {
        public override void Move(IRoom room)
        {
            if (room is ILivingroom)
            {
                ILivingroom livingroom = room as ILivingroom;
                livingroom.Chairs++;
            }else if (room is IBedroom)
            {
                IBedroom bedroom = room as IBedroom;
                bedroom.Chairs++;
            }
            else if (room is IKitchen)
            {
                IKitchen kitchen = room as IKitchen;
                kitchen.Chairs++;
            }
            
        }

        public override string Name 
        {
            get { return "Chair"; }
        }
    }
}
