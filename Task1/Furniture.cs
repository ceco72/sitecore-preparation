﻿using System;
using System.Collections.Generic;
using System.Text;
using Tasks.Contracts;

namespace Tasks
{
    public abstract class Furniture 
    {

        public abstract void Move(IRoom room);
        public abstract string Name { get; }
       
    }


}
