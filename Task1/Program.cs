﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using Tasks.Contracts;

namespace Tasks
{
    /*Random moving furniture
Topics: Inheritance, polymorphism, generic, collections, 
Implement a simple C# console application. Idea is to have a collection of furniture,
    which can be moved. Each object should have an appropriate method (public interface)
    for moving. Note that some objects can have additional methods,
    for a TV it will be turn on/turn off etc. Create a collection of objects and move
    them in a random position in a foreach. 

Additional notes:
1. It should be easy to add a new object without changing too much code.
2. There should a check that we can add only an object that has the X, Y properties and the Move method.
3. Consider using a loop to move all the objects.
4. The UI must be as simple as possible.A console application with text output is the best solution
5. Add a few additional rooms.Each room must have a name. Consider storing rooms in a 
    dictionary with the name as a key and the room as a value.
6. The TV needs to be turned off before it can be moved. */
    class Program
    {
        public static Dictionary<string, IRoom> Rooms = new Dictionary<string, IRoom>();

        static void Main(string[] args)
        {
            IBedroom bedroom1 = new Bedroom();
            IBedroom bedroom2 = new Bedroom();
            IKitchen kitchen = new Kitchen();
            ILivingroom livingroom = new Livingroom();
            
            //FOUR-ROOM APARTMENT
            Rooms.Add("Bedroom1", bedroom1);
            Rooms.Add("Bedroom2", bedroom2);
            Rooms.Add("Kitchen", kitchen);
            Rooms.Add("Livingroom", livingroom);
           
            ICollection<Furniture> myFurniture = new Collection<Furniture>();

            Console.WriteLine("Choose items (Bed, Couch, TV, Chair...) for moving.\n\rWrite End if finished. ");
            string item = Console.ReadLine();
            
            //LIST OF FURNITURE TO MOVE - USER INPUT
            while (item != "End".ToLower())
            {
                switch (item.ToLower())
                {
                    case "bed":
                        Bed mybed = new Bed();
                        myFurniture.Add(mybed);
                        break;
                    case "couch":
                        Couch mycouch = new Couch();
                        myFurniture.Add(mycouch);
                        break;
                    case "tv":
                        TV myTv = new TV();
                        myFurniture.Add(myTv);
                        break;
                    case "chair":
                        Chair myChair = new Chair();
                        myFurniture.Add(myChair);
                        break;
                    case "table":
                        Table myTable = new Table();
                        myFurniture.Add(myTable);
                        break;
                    default:
                        Console.WriteLine("Please enter one of the items.");
                        break;
                }
                Console.WriteLine("Choose items (Bed, Couch, TV, Chair) for moving.\n\rWrite End if finished. ");
                item = Console.ReadLine();
                if (item.ToLower() == "end")
                {
                    break;
                }
            }
            
            //MOVING FURNITURE TO ROOMS OF USER'S CHOICE
            foreach (var item1 in myFurniture)
            {
                if (item1.Name == "TV")
                {
                    MoveToLocation(bedroom1, bedroom2, kitchen, livingroom, item1);
                    Console.WriteLine();
                }
                if (item1.Name == "Couch")
                {
                    MoveToLocation(bedroom1, bedroom2, kitchen, livingroom, item1);
                    Console.WriteLine();
                }
                if (item1.Name == "Bed")
                {
                    MoveToLocation(bedroom1, bedroom2, kitchen, livingroom, item1);
                    Console.WriteLine();
                }
                if (item1.Name == "Chair")
                {
                    MoveToLocation(bedroom1, bedroom2, kitchen, livingroom, item1);
                    Console.WriteLine();
                }

            }
            
            //MOVER'S REPORT
            Console.WriteLine($"Livingroom has {livingroom.Chairs} chairs, {livingroom.TVs} TVs, {livingroom.Couches} couches and {livingroom.Beds} beds");
            Console.WriteLine($"Bedroom1 has {bedroom1.Chairs} chairs, {bedroom1.TVs} TVs, {bedroom1.Couches} couches and {bedroom1.Beds} beds");
            Console.WriteLine($"Bedroom2 has {bedroom2.Chairs} chairs, {bedroom2.TVs} TVs, {bedroom2.Couches} couches and {bedroom2.Beds} beds");
            Console.WriteLine($"Kitchen has {kitchen.Chairs} chairs, {kitchen.TVs} TVs, {kitchen.Couches} couches and {kitchen.Beds} beds");

            Console.ReadLine();
        }

        //MOVER AT WORK (MOIVNG FURNITURE METHOD)
        private static void MoveToLocation(IBedroom bedroom1, IBedroom bedroom2, IKitchen kitchen, ILivingroom livingroom, Furniture item1)
        {
            Console.WriteLine($"Where shall this {item1.Name} go? Livingroom: 1, Bedroom1: 2, Bedroom2: 3 Kitchen: 4");
            int location = int.Parse(Console.ReadLine());
            switch (location)
            {
                case 1:
                    item1.Move(livingroom);
                    Console.WriteLine($"{item1.Name} is moved to the Livingroom.");
                    break;
                case 2:
                    item1.Move(bedroom1);
                    Console.WriteLine($"{item1.Name} is moved to Bedroom1.");
                    break;
                case 3:
                    item1.Move(bedroom2);
                    Console.WriteLine($"{item1.Name} is moved to the Bedroom2.");
                    break;
                case 4:
                    item1.Move(kitchen);
                    Console.WriteLine($"{item1.Name} is moved to the Kitchen.");
                    break;
                default:
                    break;

            }
        }
    }
}
