﻿using System;
using System.Collections.Generic;
using System.Text;
using Tasks.Contracts;

namespace Tasks
{

    public class Couch : Furniture
    {
               
        public override void Move(IRoom room)
        {
            if (room is ILivingroom)
            {
                ILivingroom livingroom = room as ILivingroom;
                livingroom.Couches++;
            }
            else if (room is IBedroom)
            {
                IBedroom bedroom = room as IBedroom;
                bedroom.Couches++;
            }
            else if (room is IKitchen)
            {
                IKitchen kitchen = room as IKitchen;
                kitchen.Couches++;
            }
        }

        public override string Name
        {
            get
            {
                return "Couch";
            }
        }

    }
}