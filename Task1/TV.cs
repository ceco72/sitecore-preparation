﻿using System;
using System.Collections.Generic;
using System.Text;
using Tasks.Contracts;

namespace Tasks
{
    public class TV : Furniture
    {
        public bool TVOn { get; set; } = true;
        public override string Name
        {
            get { return "TV"; }
        }

        public override void Move(IRoom room)
        {
            if (room is ILivingroom)
            {
                ILivingroom livingroom = room as ILivingroom;
                livingroom.TVs++;
            }
            else if (room is IBedroom)
            {
                IBedroom bedroom = room as IBedroom;
                bedroom.TVs++;
            }
            else if (room is IKitchen)
            {
                IKitchen kitchen = room as IKitchen;
                kitchen.TVs++;
            }
        }
        public void TurnOff ()
        {
            this.TVOn = false;
            Console.WriteLine("--TV is turned off--");
        }
        public void TurnOnn()
        {
            this.TVOn = true;
            Console.WriteLine("--TV is turned onn--");
        }
    }
}
