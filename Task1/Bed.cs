﻿using System;
using System.Collections.Generic;
using System.Text;
using Tasks.Contracts;

namespace Tasks
{
    public class Bed : Furniture
    {
        public override string Name
        {
            get { return "Bed"; }
        }

        public override void Move(IRoom room)
        {
            if (room is ILivingroom)
            {
                ILivingroom livingroom = room as ILivingroom;
                livingroom.Beds++;
            }
            else if (room is IBedroom)
            {
                IBedroom bedroom = room as IBedroom;
                bedroom.Beds++;
            }
            else if (room is IKitchen)
            {
                IKitchen kitchen = room as IKitchen;
                kitchen.Beds++;
            }
        }
    }
}
