﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Security.Models
{
    public class User
    {
        [Key]
        public int UserUd { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        [ForeignKey ("RoleId")]
        public UserRole Role { get; set; }
        public User()
        {
            RoleId = 1;
        }
    }
}