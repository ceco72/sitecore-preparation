﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using System.Xml.Linq;

namespace BookXML.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
        public ActionResult GetAllBooks()
        {
            string fa = Helpers.XMLHelper.GetAllBooks();

            return View(model: fa);
        }
        public ActionResult Authors()
        {
            var fa = Helpers.XMLHelper.Authors();
            return View(model: fa);
        }
        public ActionResult ProgrammingBooks()
        {
            var fa = Helpers.XMLHelper.ProgrammingBooks();
            return View(model: fa);
        }
        public ActionResult PriceLessThan20(int price)
        {
            //implement parameter
            var fa = Helpers.XMLHelper.PriceLessThan20(price);
            return View(model: fa);
        }
        public ActionResult SelectBook(string choice)
        {

            Helpers.XMLHelper.SelectB(choice);
            return this.RedirectToAction(actionName: "Home", controllerName: "Index");
        }
    }
}