﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookXML.Models
{
    public class Book
    {
        public string Author { get; set; }  
        public string Title { get; set; }   
        public string Category { get; set; }    
        public double Price { get; set; }   
    }
}