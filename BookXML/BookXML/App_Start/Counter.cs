﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace BookXML.App_Start
{
    public class Counter : IHttpHandler
    {
        static int CountRequest = 0;
        public RequestContext RequestContext { get; set; }
        public Counter(RequestContext requestContext)
        {
            RequestContext = requestContext;
        }

        public bool IsReusable 
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.RawUrl.Contains("Home.aspx"))
            {
                CountRequest++;
            }
        }
    }
}