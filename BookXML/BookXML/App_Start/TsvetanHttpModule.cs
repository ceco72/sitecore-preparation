﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BookXML.App_Start
{
    public class TsvetanHttpModule : IHttpModule
    {
       private StreamWriter sw;
        
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(this.Application_BeginRequest);
            
        }

        private void Application_BeginRequest(Object source, EventArgs e)
        {
            if (!File.Exists("Logger.txt"))
            {
                sw = new StreamWriter(@"C:\Users\ivan\Desktop\Practice\BookXML\BookXML\Logger.txt");
            }
            else
            {
                sw = File.AppendText("Logger.txt");
            }
            sw.WriteLine("User sent request at {0}", DateTime.Now);
            sw.Close();
        }
    }
}