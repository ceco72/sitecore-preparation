﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace BookXML.App_Start
{
    public class IDGCustomHttpHandler : IHttpHandler
    {
       public RequestContext RequestContext { get; set; }
        public IDGCustomHttpHandler(RequestContext requestContext)
        {
            RequestContext = requestContext;
        }

        bool IHttpHandler.IsReusable
        {
            get { return true; }
        }

        void IHttpHandler.ProcessRequest(HttpContext context)
        {
            //context.Request.Headers
            context.Response.Write("<html><body>");
            context.Response.Write("Hello!");
            context.Response.Write("</body></html>");
            
        }
    }
}