﻿using BookXML.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace BookXML.Helpers
{
    public static class XMLHelper
    {
        public static string path = "C:\\Users\\ivan\\Desktop\\Practice\\BookXML\\BookXML\\XML\\XML.xml";
        private static XDocument xDoc;
         static XMLHelper ()
        {
            xDoc = XDocument.Load(path);
        }
        
        public static string GetAllBooks()
        {
            
            var fa = xDoc.XPathSelectElements("//book");
            var ta = JsonConvert.SerializeObject(fa);
            return ta;
        }
        public static string Authors()
        {
            IEnumerable<XElement> authors1 = xDoc.XPathSelectElements("//author");
            var ta = JsonConvert.SerializeObject(authors1);
            return ta; 
        }
        public static string ProgrammingBooks()
        {
            var programBooks = xDoc.XPathSelectElements("//book[category = 'Computer']");
            var ta = JsonConvert.SerializeObject(programBooks);
            return ta;
        }
        public static string PriceLessThan20(int price)
        {
            var priceLessThan20 = xDoc.XPathSelectElements($"//book[price < {price}]");
            var ta = JsonConvert.SerializeObject(priceLessThan20);
            return ta;
        }
        public static void SelectB(string bookTitle)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(path);
            //var selectedBook = po.XPathSelectElements($"//book[title = {bookTitle}]");

            foreach (XmlNode xNode in xdoc.SelectNodes("catalog/book"))
            {
                if (xNode.SelectSingleNode("//title").InnerText == bookTitle )
                {
                    xNode.ParentNode.RemoveChild(xNode);
                }
            }

            xdoc.Save("C:\\Users\\ivan\\Desktop\\Practice\\BookXML\\BookXML\\XML\\XML.xml");
        }
    }
}