﻿using System;

namespace Country2
{
    public class Class1
    {
        public int TaxValue(string country, out int addTax)
        {
            if (country == "usa")
            {
                addTax = 3;
                return 10;
            }   
            if (country == "ukraine")
            {
                addTax=4;
                return 15;
            }
            if (country == "norway")
            {
                addTax = 5;
                return 30;
            }
            else
            {
                addTax = 0;
                return 0;
            }
        }

    }
}
