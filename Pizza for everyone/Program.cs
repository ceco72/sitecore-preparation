﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pizza_for_everyone
{
/*There is a birthday in a company.Someone bought a big pizza with 30 pieces.However,
 as the birthday boy is a hardcore software developer, he developed a few rules:
 It is possible for anyone to take any piece at any time.Even the same one at the same time.
 Everyone must log his/her name in the file in the following way:
 Name, number of taken pieces.
 Implement a console application that simulates this situation using threads. Let’s have 4 people.
 Additional notes:
 Make sure that the log files are not corrupted.
 Make an exception-safe application.*/


    class Program
    {
        static readonly string path = @"C:\Users\ivan\Desktop\Practice\SCPrep\Record2.txt";

        static int pizzaSlices = 30;
        static int counter1 = 0;
        static int counter2 = 0;
        static int counter3 = 0;
        static int counter4 = 0;

        public static void Eating(int num)
        {
            Random rand = new Random();
            int t = rand.Next(20000, 40000);
            //A developer takes a pizza slice
            //...and starts eating/not sleeping
            while (pizzaSlices > 0)
            {
                string record;
                Thread.Sleep(t/num/2);
                Console.WriteLine($"Developer{num} has just taken a slice");
                Interlocked.Decrement(ref pizzaSlices);
                if (num==1)
                {
                    counter1++;
                    record = $"Developer{num} has taken slice";
                }
                else if(num==2)
                {
                    counter2++;
                    record = $"Developer{num} has taken slice";
                }
                else if (num==3)
                {
                    counter3++;
                    record = $"Developer{num} has taken slice";
                }
                else
                {
                    counter4++;
                    record = $"Developer{num} has taken slice";
                }
                lock (path)
                {

                    File.AppendAllText(@"C:\Users\ivan\Desktop\Practice\SCPrep\Record2.txt", record + Environment.NewLine);
                }
                Thread.Sleep(t/num);
            }
        }
        static void Main(string[] args)
        {
            for (int i = 0; i < 4; i++)
            {
                Task.Run(() =>
                {
                    Eating(i);
                });
            }
            while (true)
            {
                try
                {
                Task dev1 = Task.Run(() =>
                {
                    Eating(1);
                });
                Task dev2 = Task.Run(() =>
                {
                    Eating(2);
                }); 
                Task dev3 = Task.Run(() =>
                {
                    Eating(3);
                }); 
                Task dev4 = Task.Run(() =>
                {
                    Eating(4);
                });

                }
                catch (AggregateException)
                {
                    Console.WriteLine("Aggregate exception"); ;
                }

                if (pizzaSlices==0)
                {

                    Console.WriteLine($"Pizza slices left: {pizzaSlices}");

                    File.AppendAllText(@"C:\Users\ivan\Desktop\Practice\SCPrep\Record2.txt", "Developer1 - " + counter1 + Environment.NewLine);
                    File.AppendAllText(@"C:\Users\ivan\Desktop\Practice\SCPrep\Record2.txt", "Developer2 - " + counter2 + Environment.NewLine);
                    File.AppendAllText(@"C:\Users\ivan\Desktop\Practice\SCPrep\Record2.txt", "Developer3 - " + counter3 + Environment.NewLine);
                    File.AppendAllText(@"C:\Users\ivan\Desktop\Practice\SCPrep\Record2.txt", "Developer4 - " + counter4 + Environment.NewLine);

                    //Open the stream and read it back.
                    using (FileStream fs = File.OpenRead(path))
                    {
                        byte[] b = new byte[1024];
                        UTF8Encoding temp = new UTF8Encoding(true); 
                        int readLen;
                        while ((readLen = fs.Read(b, 0, b.Length)) > 0)
                        {
                            Console.WriteLine(temp.GetString(b, 0, readLen));
                        }
                    }

                    break;
                }
            }

        }
    }
}
