﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace France.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetImage()
        {
            var dir = Server.MapPath("/Images/grossgasteiger.jpg"); 
            return base.File(dir, "image/jpg");
        }

        //public ActionResult GetImage(string id)
        //{
        //    var dir = Server.MapPath("/Images");
        //    var path = Path.Combine(dir, id + ".jpg");
        //    return base.File(path, "image/jpeg");
        //}
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}