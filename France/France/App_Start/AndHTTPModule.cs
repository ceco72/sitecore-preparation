﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Xml.Linq;

namespace France.App_Start
{
    public class AndHTTPModule : IHttpModule
    {
        private StreamWriter sw;
        string fullPath = @"C:\Users\ivan\Desktop\Practice\France\France\Logger.txt";

        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(this.Application_BeginRequest);
            context.EndRequest += new EventHandler(this.Application_EndRequest);

        }

        private void Application_EndRequest(Object source, EventArgs e)
        {
            File.AppendAllText(fullPath, DateTime.Now.ToString() + Environment.NewLine);

        }
        private void Application_BeginRequest(Object source, EventArgs e)
        {
            File.AppendAllText(fullPath, DateTime.Now.ToString() + Environment.NewLine);

            //if (!File.Exists("Logger.txt"))
            //{
            //    sw = new StreamWriter(@"C:\Users\ivan\Desktop\Practice\France\France\Logger.txt");
            //}
            //else
            //{
            //    sw = File.AppendText("Logger.txt");
            //}
            
            //sw.WriteLine("User sent request at {0}", DateTime.Now);
            //sw.Close();
        }
    }
}