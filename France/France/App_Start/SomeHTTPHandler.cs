﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace France.App_Start
{
    public class SomeHttpHandler : IHttpHandler
    {
        public RequestContext requestContext;

        public SomeHttpHandler(RequestContext requestContext)
        {
            this.requestContext = requestContext;
        }
    
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var collection = context.Request.Headers;
            var re = collection["sec-ch-ua-platform"];
            //sec-ch-ua-platform: "Windows"

            int segN = context.Request.Url.Segments.Length;
            var info = context.Request.UrlReferrer;
            var requestedPage = context.Request.Url.LocalPath.ToLower();
            if (requestedPage == "/home/getimage" && re == "\"Windows\"") context.Response.Redirect("~/home");
           

           
                //https://localhost/HttpHandler/test.sample

            
        }
    }
}