﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CountryTax;

namespace Task1._2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Which country? ");
            string country = Console.ReadLine().ToLower();
            Console.WriteLine("Salary? ");
            int salary = int.Parse(Console.ReadLine());
            //Console.WriteLine("Additional tax value: ");
            //int addTax;
            Console.WriteLine("Salary with Tax: {0}", TaxSalary(country, salary));
            Console.ReadLine();
        }
        public static int TaxSalary(string country, int sal)
        {
            Class1 cal = new Class1();
            int addTax;
            int result = cal.TaxValue(country, out addTax) * sal / 100 + sal;
            Console.WriteLine($"Additional tax for {country} is {addTax}");
            return result;
        }
    }
    
}
