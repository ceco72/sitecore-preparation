﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Task_2._2
{
    //There are 2 objects:
    //1.A traffic light. It has the following characteristics:
    //  a.There is an enum with 2 possible values of it (Red light –denied, Green light –allowed)
    //  b.There is a method to change the light
    //  c.Every time the light color is changed, an event with appropriate parameters is raised
    //2.A human. He may have a name or a number (as you wish).He can walk and wait.
    //  He checks whether the traffic light is changed and makes appropriate decisions.
    //
    //  Implement a logic of crossing the road for 4-5 humans and one traffic light.
    //  Sequence of passing people is not important, though there should be a queue.

    class Program
    {
        static void Main(string[] args)
        {
            Human ivan = new Human("Ivan");
            Human peter = new Human("Peter");
            List<Human> people = new List<Human>();
            people.Add(ivan);
            people.Add(peter);
            
            var lights = new Lights();
            var traffic = new Traffic();

            foreach (var person in people)
            {
                traffic.TrafficConducted += person.OnTrafficConducted;
            }

            traffic.ChangeLight(lights);




        }
    }
}
